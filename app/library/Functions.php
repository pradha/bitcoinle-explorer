<?php
use Phalcon\Mvc\User\Component;
class Functions extends Component
{
	public function curlRPC($data){
		$ch = curl_init();
		$host=$this->config->blockchain->host;
		$username=$this->config->blockchain->username;
		$password=$this->config->blockchain->password;
		$port=$this->config->blockchain->port;
		curl_setopt($ch, CURLOPT_URL, "http://".$username.":".$password."@".$host);
		curl_setopt($ch, CURLOPT_PORT, $port);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		return $result;
	}
	
	public function curlRPCExt($data,$host,$username,$password,$port){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://".$username.":".$password."@".$host);
		curl_setopt($ch, CURLOPT_PORT, $port);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		return $result;
	}
	public function curlJSON($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);

		return (object) json_decode($result);
	}
	function searchVout($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['n'] === $id) {
				return $val;
			}
		}
		return null;
	}
	function prettyPrint( $json ){
		$result = '';
		$level = 0;
		$in_quotes = false;
		$in_escape = false;
		$ends_line_level = NULL;
		$json_length = strlen( $json );

		for( $i = 0; $i < $json_length; $i++ ) {
			$char = $json[$i];
			$new_line_level = NULL;
			$post = "";
			if( $ends_line_level !== NULL ) {
				$new_line_level = $ends_line_level;
				$ends_line_level = NULL;
			}
			if ( $in_escape ) {
				$in_escape = false;
			} else if( $char === '"' ) {
				$in_quotes = !$in_quotes;
			} else if( ! $in_quotes ) {
				switch( $char ) {
					case '}': case ']':
						$level--;
						$ends_line_level = NULL;
						$new_line_level = $level;
						break;

					case '{': case '[':
						$level++;
					case ',':
						$ends_line_level = $level;
						break;

					case ':':
						$post = " ";
						break;

					case " ": case "\t": case "\n": case "\r":
						$char = "";
						$ends_line_level = $new_line_level;
						$new_line_level = NULL;
						break;
				}
			} else if ( $char === '\\' ) {
				$in_escape = true;
			}
			if( $new_line_level !== NULL ) {
				$result .= "\n".str_repeat( "\t", $new_line_level );
			}
			$result .= $char.$post;
		}

		return $result;
	}
	function ip2country($ip){
		if (strpos($ip, ']') !== false) {
			$ip =  trim(explode("]",$ip)[0],"[");
		} else {
			$ip =  explode(":",$ip)[0];
		}
		$country="";
		if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {    
			$s = file_get_contents('http://ip2c.org/'.$ip);
			switch($s[0]){
				case '1':
					$reply = explode(';',$s);
					$country=$reply[1];
					break;
			}
		} else if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {   
			/*$s = file_get_contents('https://api.ipdata.co/'.$ip.'?api-key=07bbc0ba49891f2522bfbbdbf3c54b9a971d384fb163e2dfa32447b4');*/
			//$s = file_get_contents('https://geoip.nekudo.com/api/'.$ip);
			if(false === ($s = file_get_contents('https://geoip.nekudo.com/api/'.$ip))){
				$country="";
			} else {
				$reply=json_decode($s,true);
				$country=$reply["country"]["code"];
			}
			
		}
		echo "country result: ".$ip.PHP_EOL;
		return $country;
	}
	function addrAlias($addr=null){ 
		if ($addr!=null){
			$address=Addresses::findFirst([
				'conditions' => ['address' => $addr],
				'limit' => 1
			]);
			if (isset($address->alias) && $address->alias!=""){
				return $address->alias;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function secondsToDate($seconds){
		$dtF = new DateTime('@0');
		$dtT = new DateTime("@".$seconds);
		if ($dtF->diff($dtT)->format('%a')==0 && $dtF->diff($dtT)->format('%h')==0 && $dtF->diff($dtT)->format('%i')==0)
			$activeseconds=$dtF->diff($dtT)->format('%s second ago');
		else if ($dtF->diff($dtT)->format('%a')==0 && $dtF->diff($dtT)->format('%h')==0)
			$activeseconds=$dtF->diff($dtT)->format('%im:%ss');
		else if ($dtF->diff($dtT)->format('%a')==0)
			$activeseconds=$dtF->diff($dtT)->format('%hh%im%ss');
		else 
			$activeseconds=$dtF->diff($dtT)->format('%ad, %hh%im%ss');
		return $activeseconds;
	}
}
