<?php

class BleController extends \Phalcon\Mvc\Controller
{
	var $function;
	var $subUrl="";
	public function initialize()
    {
        $this->view->title=$this->config->application->name;
		$this->view->unit=$this->config->blockchain->ticker;
		$this->view->subUrl=$this->subUrl="/ble";
		$this->function=$this->view->function=new Functions();
		$this->view->setLayout('stats');
		$this->view->setTemplateAfter('layout');
		
		$blocks=Transactions::find([
					'sort'=>[
						'blockheight' => -1
					],
					'limit' => 350
				]);
				
		$blockTime=array();
		$blockHeight=0;
		foreach($blocks as $block){
			if ($blockHeight!=$block->blockheight)
				array_push($blockTime,$block->time);
				$blockHeight=$block->blockheight;
		}
				
		$timeBefore=0;
		$timeCount=0;
		foreach($blockTime as $time){
			if ($timeBefore==0)
				$timeBefore=$time;
			else {
				if ($time!=$timeBefore)
					$timeCount+=($timeBefore-$time);
					$timeBefore=$time;
			}
		}
				
		$this->view->blockTimeCount=count($blockTime);
		$this->view->blockTimeAVG=$timeCount/count($blockTime);
				
		$this->view->lastBlock=$lastBlock=Transactions::findFirst([
			'sort'=>[
				'blockheight' => -1
			],
			'limit' => 1
		]);
		
		$data["method"]="getnetworkhashps";
		$data["params"]=array();
		$this->view->hashrate= json_decode($this->function->curlRPC($data),true)["result"];
				
		$this->view->mnCount=Masternodes::count();
		$this->view->mnEnabled=Masternodes::count([
			'conditions' => ['status' => 'ENABLED']
		]);
			
		$data["method"]="getblock";
		$data["params"]=array($lastBlock->blockhash);
		$this->view->block=$result=(object) json_decode($this->function->curlRPC($data),true)["result"];
			
		/*$supply=Addresses::aggregate([
			[
				'$group' => [
					'_id' => null,
					'total' => ['$sum' => '$balance']
				]
			]
		]);
				
		foreach($supply as $supply){
			$supply=$supply;
		}*/
		
		//new way
		$supply=new stdClass;
		$supply->total=$lastBlock->blockheight*50;
		
		$this->view->supply=$supply;
		
		$this->view->price=Market::findFirst([
			'conditions' => ['coin' => $this->config->blockchain->ticker],
			'sort' => ['time' => -1]
		]);
		$this->view->prices=Market::find([
			'conditions' => ['coin' => $this->config->blockchain->ticker],
			'sort' => ['time' => -1],
			'limit' => 20
		]);
    }
    public function indexAction()
    {
		$this->function=new Functions();
		$data=array(); 
		$data["jsonrpc"]=1.0;
		
		//check last block
		$data["method"]="getblockchaininfo";
		$data["params"]=array();
		
		$result=json_decode($this->function->curlRPC($data),true);
		$lastBlock=$result["result"]["blocks"];
		
		$blockCount=$lastBlock;
		$blocks=new ArrayObject();
		$transactions=new ArrayObject();
		while ($blockCount>=$lastBlock-30){
			$data["method"]="getblockhash";
			$data["params"]=array(intval($blockCount));
			$hashBlock=(object)json_decode($this->function->curlRPC($data),true);
			
			$newBlock=new ArrayObject();
			
			$data["method"]="getblock";
			$data["params"]=array($hashBlock->result);
			$dataBlock=(object)json_decode($this->function->curlRPC($data),true)["result"];
			
			
			foreach($dataBlock->tx as $trx){
				$data["method"]="getrawtransaction";
				$data["params"]=array($trx,1);
				$dataTrx=(object)json_decode($this->function->curlRPC($data),true)["result"];
				
				$vinObject= new ArrayObject();
				foreach($dataTrx->vin as $vin){
					$newVin=new ArrayObject();
					if (!isset($vin["coinbase"])){
						$data["params"]=array($vin["txid"],1);
						$resultVin=json_decode($this->function->curlRPC($data),true)["result"];
						$newVin->address=$this->function->searchVout($vin["vout"],$resultVin["vout"])["scriptPubKey"]["addresses"][0];
						$newVin->value=$this->function->searchVout($vin["vout"],$resultVin["vout"])["value"];
						$vinObject->append($newVin);
					}
				}
				
				$newTrx=new ArrayObject();
				$newTrx->hash=$dataTrx->txid;
				$newTrx->time=$dataTrx->time;
				$recipients=0;
				$txValue=0;
				foreach($dataTrx->vout as $vout){
					$isPos=false;
					$vinValue=0;
					foreach($vinObject as $key => $value){
						if ($vout["scriptPubKey"]["type"]!="nonstandard" && $value->address==$vout["scriptPubKey"]["addresses"][0] && count($vinObject)==1 &&  $vout["scriptPubKey"]["type"]=="pubkey"){
							$isPos=true;
							$vinValue=$value->value;
							break;
						}
					}
					
					if (isset($vout["scriptPubKey"]["addresses"][0])){
						if (count($vinObject)==1 && $isPos)
							$txValue+=$vout["value"]-$vinValue;
						else 
							$txValue+=$vout["value"];
						$recipients++;
					}
				}
				$newTrx->recipients=$recipients;
				$newTrx->block=$blockCount;
				$newTrx->value=$txValue;
				
				if ($vout["scriptPubKey"]["type"]!="nonstandard")
					$transactions->append($newTrx);
			}
			
			
			$newBlock->hash=$dataBlock->hash;
			$newBlock->height=$dataBlock->height;
			$newBlock->size=$dataBlock->size;
			$newBlock->time=$dataBlock->time;
			$newBlock->txCount=count($dataBlock->tx);
			
			$blocks->append($newBlock);
			$blockCount--;
		}
		$this->view->blocks=$blocks;
		$this->view->transactions=$transactions;
		$this->view->vin=$vinObject;
		
    }
	public function qrAction($data=null){
		$this->view->disable();
		
		
		include(APP_PATH . '/library/qrcode/qrlib.php'); 
		
         $this->response->setHeader('Content-Type', 'image/png');
          echo QRcode::png($data);
		  die;
	}
	
	public function blockAction($value=null){
		$this->view->title="Block";
		if ($value!=null){
			$data["method"]="getblock";
			$data["params"]=array($value);
			$this->view->result=$result=(object) json_decode($this->function->curlRPC($data),true)["result"];
			
			$transactions=new ArrayObject();
			foreach($result->tx as $trx){
				$data["method"]="getrawtransaction";
				$data["params"]=array($trx,1);
				$dataTrx=(object)json_decode($this->function->curlRPC($data),true)["result"];
				
				$vinObject= new ArrayObject();
				foreach($dataTrx->vin as $vin){
					$newVin=new ArrayObject();
					if (!isset($vin["coinbase"])){
						$data["params"]=array($vin["txid"],1);
						$resultVin=json_decode($this->function->curlRPC($data),true)["result"];
						$newVin->address=$this->function->searchVout($vin["vout"],$resultVin["vout"])["scriptPubKey"]["addresses"][0];
						$newVin->value=$this->function->searchVout($vin["vout"],$resultVin["vout"])["value"];
						$vinObject->append($newVin);
					}
				}
				
				$newTrx=new ArrayObject();
				$newTrx->hash=$dataTrx->txid;
				$newTrx->time=$dataTrx->time;
				$recipients=0;
				$txValue=0;
				foreach($dataTrx->vout as $vout){
					$isPos=false;
					$vinValue=0;
					foreach($vinObject as $key => $value){
						if ($vout["scriptPubKey"]["type"]!="nonstandard" && $value->address==$vout["scriptPubKey"]["addresses"][0] && count($vinObject)==1 &&  $vout["value"]>$value->value){
							$isPos=true;
							$vinValue=$value->value;
							break;
						}
					}
					
					if (isset($vout["scriptPubKey"]["addresses"][0])){
						if (count($vinObject)==1 && $isPos)
							$txValue+=$vout["value"]-$vinValue;
						else 
							$txValue+=$vout["value"];
						$recipients++;
					}
				}
				$newTrx->recipients=$recipients;
				$newTrx->block=$result->height;
				$newTrx->value=$txValue;
				
				//if ($vout["scriptPubKey"]["type"]!="nonstandard")
					$transactions->append($newTrx);
			}
			
			
			$this->view->transactions=$transactions;
		}
	}
	public function addressAction($value=null){
		$this->view->title="Address";
		if ($value!=null){
			$address=Addresses::findFirst(['conditions'=>['address' => $value]]);
			if (isset($address->address)){
				$this->view->address=$address;
				$this->view->transactions=Transactions::find([
					'conditions'=> ['address' => $value],
					'sort'=>[
						'time' => -1,
						'type' => 1
					],
					'limit' => 100
				]);
				
				$trxs=Transactions::aggregate([
					[
						'$match' => [
							'address' => $value 
						]
					],
					[
						'$count' => 'total'
					]
				]);
				foreach($trxs as $trxs){
					$trxs=$trxs;
				}
				$this->view->trxCount=$trxs;
			} else {
				$this->view->pick(['error/404']);
			}
		}
	}
	public function txAction($value=null){
		$this->view->title="Transaction";
		if ($value!=null){
			$data=array();
			$data["jsonrpc"]=1.0;
			
			//get raw transaction
			$data["method"]="getrawtransaction";
			$data["params"]=array($value,1);
			
			$result=json_decode($this->function->curlRPC($data),true)["result"];
			$this->view->result=(object) $result;
			
			$data["method"]="getblock";
			$data["params"]=array($result['blockhash']);
			$this->view->block=(object) json_decode($this->function->curlRPC($data),true)["result"];
			
			$this->view->transactions=Transactions::find([
					'conditions'=> ['txid' => $result['txid']],
					'sort'=>[
						'time' => -1
					],
					'limit' => 100
				]);
				
			$vins=new ArrayObject();
			$address="";
			$vinTotal=0;
			$vinValue=0;
			if (!isset($result["vin"][0]["coinbase"])){
				foreach($result["vin"] as $vin){
					$arrVin=new ArrayObject();
					$data["method"]="getrawtransaction";
					$data["params"]=array($vin["txid"],1);
					$vinTrx=json_decode($this->function->curlRPC($data),true)["result"];
					//$this->view->disable();
					//print_r($vinTrx);
					$trx=$this->function->searchVout($vin["vout"],$vinTrx["vout"]);
			
					//search existing 
					$exist=false;
					foreach($vins as $key => $value){
						if ($value->address==$trx["scriptPubKey"]["addresses"][0]){
							$exist=true;
							$value->value+=$trx["value"];
							break;
						}
					}
					if ($exist===false){
						$arrVin->address=$trx["scriptPubKey"]["addresses"][0];
						$arrVin->value=$trx["value"];
						$vinTotal=$trx["value"];
						$vins->append($arrVin);
					}
					$address=$trx["scriptPubKey"]["addresses"][0];
					$vinValue+=$trx["value"];
				}
			} else $vins->append('coinbase');
	
			
		
			$vouts=new ArrayObject();
			$voutValue=0;
			foreach($result["vout"] as $vout){
				$arrVout=new ArrayObject();
				if ($vout["scriptPubKey"]["type"]!="nonstandard"){
					
					$exist=false;
					foreach($vouts as $key => $value){
						if ($value->address==$vout["scriptPubKey"]["addresses"][0]){
							$exist=true;
							$value->value+=$vout["value"];
							break;
						}
					}
					
					if ($exist==false){
						if ($vout["scriptPubKey"]["type"]=="pubkey"){
							foreach($vins as $key => $value){
								if ($value->address==$vout["scriptPubKey"]["addresses"][0]){
									$exist=true;
									$value->address="PoS";
									$arrVout->value=$vout["value"]-$value->value;
									$voutValue+=$vout["value"];
									break;
								} else {
									$arrVout->value=$vout["value"];
								}
							}
						} else {
							$arrVout->value=$vout["value"];
						}
						$arrVout->address=$vout["scriptPubKey"]["addresses"][0];
						$vouts->append($arrVout);
					}
					
					$voutValue+=$vout["value"];
				}
			}
			
			$this->view->vinValue=$vinValue;
			$this->view->voutValue=$voutValue;
			$this->view->vins=$vins;
			$this->view->vouts=$vouts;
			$this->view->value=$value;
			
		}
	}
	
	public function allocationAction(){
		$this->view->title="Allocation";
		$this->view->addressess=Addresses::find([
					'sort'=>[
						'balance' => -1
					],
					'limit' => 100
				]);
	}
	public function masternodeAction($tx=null){
		$this->view->title="Masternode";
		if ($tx=="data"){
			$this->view->disable();
			$data=array();
			$data["data"]=array();
			$masternodes=Masternodes::find([
					'conditions' => [ 'status' => ['$ne' => 'REMOVE']]
				]);
			foreach($masternodes as $masternode){
				$detail=array();
				$dtF = new DateTime('@0');
				$dtT = new DateTime("@".$masternode->activeseconds);
				if ($dtF->diff($dtT)->format('%a')==0 && $dtF->diff($dtT)->format('%h')==0 && $dtF->diff($dtT)->format('%i')==0)
					$activeseconds=$dtF->diff($dtT)->format('%s second ago');
				else if ($dtF->diff($dtT)->format('%a')==0 && $dtF->diff($dtT)->format('%h')==0)
					$activeseconds=$dtF->diff($dtT)->format('%im:%ss');
				else if ($dtF->diff($dtT)->format('%a')==0)
					$activeseconds=$dtF->diff($dtT)->format('%hh%im%ss');
				else 
					$activeseconds=$dtF->diff($dtT)->format('%ad, %hh%im%ss');
				$detail=array('<i title="'.$masternode->country.'" class="'.strtolower($masternode->country).' flag"></i>',$masternode->ip,$masternode->protocol,$activeseconds, '<a href="'.$this->url->get($this->subUrl.'/address/'.$masternode->pubkey).'">'.$masternode->pubkey.'</a>', $masternode->status);
				array_push($data["data"],$detail);
			}
			echo json_encode($data,true);
		} else if ($tx=="country"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			$masternodes=Masternodes::find([
					'conditions' => [ 'status' => ['$ne' => 'REMOVE']]
				]);
			$data=array();
			foreach($masternodes as $masternode){
				if (isset($data[$masternode->country]))
					$data[$masternode->country]+=1;
				else
					$data[$masternode->country]=1;
			}
			echo json_encode($data);
		} else {
			$this->view->masternodes=$masternodes=Masternodes::find([
					'conditions' => [ 'status' => ['$ne' => 'REMOVE']]
				]);
			$data=array();
			foreach($masternodes as $masternode){
				if (isset($data[$masternode->country]))
					$data[$masternode->country]+=1;
				else
					$data[$masternode->country]=1;
			}
			$this->view->country=json_encode($data);
		}
	}
	
	public function networkAction(){
		$this->view->title="Network";
		$data["method"]="getpeerinfo";
		$data["params"]=array();
		$this->view->peers=(object)json_decode($this->function->curlRPC($data),true)["result"];
	}
	public function searchAction(){
		$this->view->title="Search";
		if ($this->request->isPost()){
			$src=$this->request->getPost('src');
			
			if (is_numeric($src)){
				$data["method"]="getblockhash";
				$data["params"]=array(intval($src));
				$hashBlock=(object)json_decode($this->function->curlRPC($data),true);
				
				if (isset($hashBlock->result) && $hashBlock->result!="")
					$this->response->redirect($this->subUrl.'/block/'.$hashBlock->result);
			}
			
			//check address
			$address=Addresses::findFirst(['conditions'=>['address' => $src]]);
			if (isset($address->address))
				$this->response->redirect($this->subUrl.'/address/'.$src);
			
			//check trx
			$transaction=Transactions::findFirst(['conditions'=>['txid' => $src]]);
			if (isset($transaction->txid))
				$this->response->redirect($this->subUrl.'/tx/'.$src);
			
			//check trx
			$block=Transactions::findFirst(['conditions'=>['blockhash' => $src]]);
			if (isset($block->txid))
				$this->response->redirect($this->subUrl.'/block/'.$src);
		} else {
			$this->view->disable();
		}
	}
	
	public function statisticsAction($data=null){
		$this->view->disable();
		$this->response->setContentType('application/json', 'UTF-8');
		
			$result=array();
			if ($data=="block"){
				$blocks=Transactions::find([
					'sort'=>[
						'blockheight' => -1
					],
					'limit' => 350
				]);
				
				$blockTime=array();
				$blockHeight=0;
				foreach($blocks as $block){
					if ($blockHeight!=$block->blockheight)
						array_push($blockTime,$block->time);
					$blockHeight=$block->blockheight;
				}
				
				$timeBefore=0;
				$timeCount=0;
				foreach($blockTime as $time){
					if ($timeBefore==0)
						$timeBefore=$time;
					else {
						if ($time!=$timeBefore)
							$timeCount+=($timeBefore-$time);
						$timeBefore=$time;
					}
				}
				
				
				$result["blockTimeCount"]=count($blockTime);
				$result["blockTimeAVG"]=$timeCount/count($blockTime);
				$result["mnCount"]=Masternodes::count();
				$result["mnEnabled"]=Masternodes::count([
					'conditions' => ['status' => 'ENABLED']
				]);
				$result["block"]=$lastBlock=Transactions::findFirst([
					'sort'=>[
						'blockheight' => -1
					],
					'limit' => 1
				]);
				
				$data=array(); 
				$data["jsonrpc"]=1.0;
				$data["method"]="getblock";
				$data["params"]=array($lastBlock->blockhash);
				$blockInfo=(object) json_decode($this->function->curlRPC($data),true)["result"];
				$result["difficulty"]=$blockInfo->difficulty;
				
				$data["method"]="getnetworkhashps";
				$data["params"]=array();
				$result["hashrate"]= json_decode($this->function->curlRPC($data),true)["result"];
				
				$result["price"]=Market::findFirst([
					'conditions' => ['coin' => $this->config->blockchain->ticker],
					'sort' => ['time' => -1]
				]);
				$supply=Addresses::aggregate([
					[
						'$group' => [
							'_id' => null,
							'total' => ['$sum' => '$balance']
						]
					]
				]);
					
				foreach($supply as $supply){
					$supply=$supply;
				}
				
				//new way
				$supply=new stdClass;
				$supply->total=$lastBlock->blockheight*50;
		
				$result["supply"]=$supply;
				echo json_encode($result);
			}
		
	}
	public function apiAction($param=null, $info=null, $detail=null, $beautify=false){
		
		$data=array(); 
		$data["jsonrpc"]=1.0;
		if ($param=="price"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			if (isset($info) && is_numeric($info)){
				$prices=Market::find([
					'conditions' => ['coin' => $this->config->blockchain->ticker,'time' => [ '$gt' => (int)$info]],
					'sort' => ['time' => -1],
					'limit' => 20
				]);
			} else {
				$prices=Market::findFirst([
					'conditions' => ['coin' => $this->config->blockchain->ticker],
					'sort' => ['time' => -1],
					'limit' => 20
				]);
			}
			echo json_encode($prices);
		}
		if ($param=="diff"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			$data["method"]="getblockchaininfo";
			$raw=json_decode($this->function->curlRPC($data),true)["result"];
			echo $raw["difficulty"];
		} else if ($param=="connection"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			$data["method"]="getconnectioncount";
			$raw=json_decode($this->function->curlRPC($data),true)["result"];
			echo $raw;
		} else if ($param=="blockcount"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			$data["method"]="getblockcount";
			$raw=json_decode($this->function->curlRPC($data),true)["result"];
			echo $raw;
		} else if ($param=="blockhash"){
			$this->view->disable();		
			if ($detail=="winner"){
				$trx=Transactions::findFirst([
					'conditions'=> ['blockheight' => intval($info)],
					'sort'=>[
						'time' => -1
					],
					'limit' => 1
				]);
				$winner=Transactions::find([
					'conditions'=> ['txid' => $trx->txid,'position' => 'vout'],
					'sort'=>[
						'time' => -1
					],
					'limit' => 10
				]);
				$winners=array();
				foreach($winner as $win){
					array_push($winners,$win->address);
				}
				echo json_encode($winners);
			} else {
				$this->response->setContentType('application/json', 'UTF-8');
				$data["method"]="getblockhash";
				$data["params"]=array(intval($info));
				$raw=json_decode($this->function->curlRPC($data),true)["result"];
				echo $raw;
			}
		} else if ($param=="address"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			
			if ($info!=null){
				$address=Addresses::findFirst(['conditions'=>['address' => $info]]);
				if (isset($address->address)){
					
					$response=array();
					$trxs=Transactions::aggregate([
						[
							'$match' => [
								'address' => $value 
							]
						],
						[
							'$count' => 'total'
						]
					]);
					foreach($trxs as $trxs){
						$trxs=$trxs;
					}
					$response["address"]=$address->address;
					$response["sent"]=$address->sent;
					$response["received"]=$address->received;
					$response["balance"]=$address->balance;
					echo json_encode($response);
				} else {
					echo "failed";
				}
			} else {
				echo "failed";
			}
			
		} else if ($param=="block"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			$data["method"]="getblock";
			$data["params"]=array($info);
			$raw=json_encode(json_decode($this->function->curlRPC($data),true)["result"]);
			print_r($raw);
		} else if ($param=="transaction"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			$data["method"]="getrawtransaction";
			$data["params"]=array($info,is_numeric($detail) ? intval($detail) : 0);
			$raw=json_encode(json_decode($this->function->curlRPC($data),true)["result"]);
			print_r($raw);
		} else if ($param=="network"){
			$this->view->disable();			
			$this->response->setContentType('application/json', 'UTF-8');
			$data["method"]="getnetworkhashps";
			$raw=json_decode($this->function->curlRPC($data),true)["result"];
			echo $raw;
		} else if ($param=="supply"){
			$this->view->disable();			
			$this->response->setContentType('application/json', 'UTF-8');
			$supply=Addresses::aggregate([
					[
						'$group' => [
							'_id' => null,
							'total' => ['$sum' => '$balance']
						]
					]
				]);
					
				foreach($supply as $supply){
					$supply=$supply;
				}
			$response=array();
			$response["account_balance"]=$supply->total;
			
			$data["method"]="getblockcount";
			$block=json_decode($this->function->curlRPC($data),true)["result"];
			$response["logic_math"]=$block*50;
			
			echo json_encode($response);
		} else if ($param=="allocation"){
			$this->view->disable();			
			$this->response->setContentType('application/json', 'UTF-8');
			$addresses=Addresses::find([
					'sort'=>[
						'balance' => -1
					],
					'limit' => 100
				]);
			$response=new ArrayObject();
			$i=1;
			foreach($addresses as $address){
				if (!isset($response[$i]))
					$response[$i]=new stdClass;
				$response[$i]->address=$address->address;
				$response[$i]->sent=$address->sent;
				$response[$i]->received=$address->received;
				$response[$i]->balance=$address->balance;
				$i++;
			}
			echo json_encode($response);
		} else if ($param=="blockData"){
			$this->view->disable();
			$this->response->setContentType('application/json', 'UTF-8');
			
			//check last block
			$data["method"]="getblockchaininfo";
			$data["params"]=array();
			
			$getInfo=(object)json_decode($this->function->curlRPC($data),true)["result"];
			if ($info!=null && is_numeric($info) && $info!=0)
				$currentBlock=$info+1;
			else 
				$currentBlock=$getInfo->blocks-30;
			
			$result=array();
			$result["block"]=array();
			$result["transaction"]=array();
			if ($currentBlock<$getInfo->blocks){
				while ($currentBlock<=$getInfo->blocks){
					$arrBlock=array();
					$data["method"]="getblockhash";
					$data["params"]=array(intval($currentBlock));
					$hashBlock=json_decode($this->function->curlRPC($data),true)["result"];
					
					$data["method"]="getblock";
					$data["params"]=array($hashBlock);
					$block=(object) json_decode($this->function->curlRPC($data),true)["result"];
					
					$arrBlock["hash"]=$block->hash;
					$arrBlock["size"]=$block->size;
					$arrBlock["time"]=$block->time;
					$arrBlock["height"]=$block->height;
					$arrBlock["tx"]=count($block->tx);
					
					array_push($result["block"],$arrBlock);
					
					foreach($block->tx as $trx){
						$data["method"]="getrawtransaction";
						$data["params"]=array($trx,1);
						$dataTrx=(object)json_decode($this->function->curlRPC($data),true)["result"];
						
						$vinObject= new ArrayObject();
						foreach($dataTrx->vin as $vin){
							$newVin=new ArrayObject();
							if (!isset($vin["coinbase"])){
								$data["params"]=array($vin["txid"],1);
								$resultVin=json_decode($this->function->curlRPC($data),true)["result"];
								$newVin->address=$this->function->searchVout($vin["vout"],$resultVin["vout"])["scriptPubKey"]["addresses"][0];
								$newVin->value=$this->function->searchVout($vin["vout"],$resultVin["vout"])["value"];
								$vinObject->append($newVin);
							}
						}
						
						$newTrx=array();
						$newTrx["hash"]=$dataTrx->txid;
						$newTrx["time"]=$dataTrx->time;
						$recipients=0;
						$txValue=0;
						$addrPrev="";
						foreach($dataTrx->vout as $vout){
							$isPos=false;
							$vinValue=0;
							foreach($vinObject as $key => $value){
								if ($vout["scriptPubKey"]["type"]!="nonstandard" && $value->address==$vout["scriptPubKey"]["addresses"][0] && count($vinObject)==1 &&  $vout["scriptPubKey"]["type"]=="pubkey"){
									$isPos=true;
									$vinValue=$value->value;
									break;
								}
							}
							if (isset($vout["scriptPubKey"]["addresses"][0])){
								if (count($vinObject)==1 && $isPos){
									if ($addrPrev!=$vout["scriptPubKey"]["addresses"][0])
										$txValue+=$vout["value"]-$vinValue;
									else
										$txValue+=$vout["value"];
								} else 
									$txValue+=$vout["value"];
								$recipients++;
								$addrPrev=$vout["scriptPubKey"]["addresses"][0];
							}
						}
						$newTrx["recipients"]=$recipients;
						$newTrx["block"]=$currentBlock;
						$newTrx["value"]=$txValue;
						
						if ($vout["scriptPubKey"]["type"]!="nonstandard")
							array_push($result["transaction"],$newTrx);
						
						
					}
					
					$currentBlock++;
				}
				echo json_encode($result);
			} else {
				echo "up to date";
			}
		} else {
			$this->view->title="Explorer API";
		}
	}
	
	public function faucetAction($param=null){
		$this->view->title="BitcoinLE Faucet";
		
		//get balance
		$data=array(); 
		$data["jsonrpc"]=1.0;
		$data["method"]="getbalance";
		$this->view->balance=(object)json_decode($this->function->curlRPC($data),true);
				
		$this->view->lists=Faucet::find([
			'sort'=>[   
				'time' => -1
			],
			'limit' => 10
		]);
		if ($this->request->isPost()){
			$this->view->disable();
			$address=$this->request->getPost('address');
			$clientIp=$this->request->getClientAddress();
			$exist=false;
			
			//check
			$check=Faucet::findFirst([
				[
					'$or' => [
						['address'	=>	$value],
						['client'	=> $clientIp]
					]
				],
				'sort'=>[
					'time' => -1
				],
				'limit' => 1
			]);
			if (isset($check->id))
				$exist=true;
			
			$response=new stdClass;
			$response->status=false;
			$gResponse=$this->request->getPost('captcha');
			
			//check chaptcha
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,"secret=6Lc1i3EUAAAAAIcY8Y37cP6lNKEFer28fM76HWT8&response=".$gResponse."&remoteip=".$clientIp);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$googleResponse = json_decode(curl_exec($ch));
			curl_close ($ch);
			
			
			if ($googleResponse->success==false){
				$response->message="Failed google recaptcha";
			} else if ($exist && $check->time > (time() - 60*60*12)){
				
					$period=($check->time+(60*60*12))-time();
					$hours=gmdate("H", $period);
					if ($hours>=2) $hr="hours"; else $hr="hour";
					$minutes=gmdate("i", $period);
					if ($minutes>=2) $mn="minutes"; else $mn="minute";
					$seconds=gmdate("s", $period);
					if ($seconds>=2) $sc="seconds"; else $mn="second";
					
					$time="";
					if ($hours<=0)
						$time=$minutes." ".$mn." and ".$seconds." ".$sc;
					else if ($hours<=0 && $minutes<=0)
						$time=$seconds." ".$sc;
					else 
						$time=$hours." ".$hr.", ".$minutes." ".$mn." and ".$seconds." ".$sc;
					$response->exist=true;
					$response->last=$check->time;
					$response->message="Please wait for ".$time;
			} else if (trim($address)==""){
				$response->address=$address;
				$response->message="Please fill valid BLE address";
			} else {
				$amount=rand(1,5)/1000;
				//send BLE
				$data=array(); 
				$data["jsonrpc"]=1.0;
				$data["method"]="sendtoaddress";
				$data["params"]=array($address,$amount);
				$getTrx=(object)json_decode($this->function->curlRPC($data),true);
				
				if ($getTrx->result!=null){
					$random = new \Phalcon\Security\Random();
					
					$faucet=new Faucet();
					$faucet->id=$random->uuid();
					$faucet->address=$address;
					$faucet->client=$clientIp;
					$faucet->txid=$getTrx->result;
					$faucet->amount=$amount;
					$faucet->time=time();
					$faucet->save();
					
					$response->status=true;
					$response->amount=$amount;
					$response->message="success";
					$response->time=time();
					$response->txid=$getTrx->result;
				} else {
					$response->message=$getTrx->error["message"];
					$response->detail=$getTrx;
					$response->data=$data;
				}
			}
			echo json_encode($response);
		}
	}
	
	public function poolAction(){
		$this->view->title="BitcoinLE Pool";
		//$this->view->disable();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://blepool.com/api/pool/BLE/");
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$this->view->poolResponse = $poolResponse= (object)json_decode(curl_exec($ch));
		curl_close ($ch);
	
		if ($this->request->isPost()){
			$this->view->disable();
			$account=$this->request->getPost('account');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://blepool.com/api/pool/BLE/");
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$poolResponse= (object)json_decode(curl_exec($ch));
			curl_close ($ch);
			
			$miners=new ArrayObject();
			$i=0;
			foreach($poolResponse->miners->list as $miner){
				if ($miner->username==$account){
					if (!isset($miners[$i]))
						$miners[$i]=new stdClass;
					$miners[$i]->id=$miner->id;
					$miners[$i]->username=$miner->username;
					$miners[$i]->difficulty=$miner->difficulty;
					$i++;
				}
			}
			echo json_encode($miners);
		}
	}
	
	public function meritAction($check=false){
		if ($check){
			$this->view->disable();
			$dir = './files/btalk/';
			$files = array_diff(scandir($dir), array('.', '..'));
			echo count($files);
		} else {
			$this->view->title="BitcoinLE BTalk Merit Faucet";
			$dir = './files/btalk/';
			$this->view->files = $files = array_diff(scandir($dir), array('.', '..'));
		}
	}
	
	
	public function btalkAction($phase=1, $data=null){
		require APP_PATH . '/library/simple_html_dom.php';
		$this->view->disable();
		$this->response->setContentType('application/json', 'UTF-8');
		
		$multiplier=($phase*100)+(($phase-1)*400);
		$pages=array(($multiplier/5)-20, $multiplier/5, ($multiplier/5)+20, ($multiplier/5)+40, ($multiplier/5)+60);
		$key=0;
		
		$threads=new ArrayObject();
		
		foreach ($pages as $page){
			$html = file_get_html('https://bitcointalk.org/index.php?topic=4951492.'.$page);
			
			$form=str_get_html($html->find('form',1));
			$threadClass=$form->find('tr',0)->class;
			
			foreach($form->find('td[class=windowbg],td[class=windowbg2]') as $element) {
				$thread=str_get_html($element);
				if (!isset($threads[$key]))
					$threads[$key]=new stdClass;
				$threads[$key]->user=$thread->find('td[class=poster_info]',0)->first_child()->childNodes(0)->plaintext;
				$threads[$key]->href=$thread->find('td[class=poster_info]',0)->first_child()->childNodes(0)->attr['href'];
				
				
				if ($thread->find('td[class=td_headerandpost]',0)->first_child()->childNodes(0)->childNodes(1)){
				
					//check duplicate
					$exist=false;
					foreach($threads as $k => $v){
						if (isset($threads[$k]->num) && $threads[$k]->num==trim(str_replace("&nbsp;", "", htmlentities($thread->find('td[class=td_headerandpost]',0)->first_child()->childNodes(0)->childNodes(2)->childNodes(0)->plaintext)))){
							$exist=true;
							break;
						}
					}
					
					if (!$exist){
						$threads[$key]->title=$thread->find('td[class=td_headerandpost]',0)->first_child()->childNodes(0)->childNodes(1)->childNodes(0)->childNodes(0)->plaintext;
						$threads[$key]->post=$thread->find('div[class=post]',0)->plaintext;
						
						if (preg_match_all('/[0-9a-zA-Z]{27,34}@BLE/', $threads[$key]->post))
							preg_match_all('/[0-9a-zA-Z]{27,34}@BLE/', $threads[$key]->post, $matches);
						if (preg_match_all('/BLE\.[0-9a-zA-Z]{27,34}/', $threads[$key]->post))
							preg_match_all('/BLE\.[0-9a-zA-Z]{27,34}/', $threads[$key]->post, $matches);
						if (preg_match_all('/BLE\:[0-9a-zA-Z]{27,34}/', $threads[$key]->post))
							preg_match_all('/BLE\:[0-9a-zA-Z]{27,34}/', $threads[$key]->post, $matches);
						if (preg_match_all('/BLE\ [0-9a-zA-Z]{27,34}/', $threads[$key]->post))
							preg_match_all('/BLE\ [0-9a-zA-Z]{27,34}/', $threads[$key]->post, $matches);
							
						if (isset($matches)){
							if (preg_match('/[0-9a-zA-Z]{27,34}@BLE/', end($matches[0]))){
								$address=explode("@",end($matches[0]));
								$threads[$key]->address=$address[0];
							}
							if (preg_match('/BLE\.[0-9a-zA-Z]{27,34}/', end($matches[0]))){
								$address=explode(".",end($matches[0]));
								$threads[$key]->address=end($address);
							}
							if (preg_match('/BLE\:[0-9a-zA-Z]{27,34}/', end($matches[0]))){
								$address=explode(":",end($matches[0]));
								$threads[$key]->address=end($address);
							}
							if (preg_match('/BLE\ [0-9a-zA-Z]{27,34}/', end($matches[0]))){
								$address=explode(" ",end($matches[0]));
								$threads[$key]->address=end($address);
							}
						}
						
						unset($matches);
						$threads[$key]->time=strtotime($thread->find('td[class=td_headerandpost]',0)->first_child()->childNodes(0)->childNodes(1)->childNodes(1)->plaintext);
						$threads[$key]->num=trim(str_replace("&nbsp;", "", htmlentities($thread->find('td[class=td_headerandpost]',0)->first_child()->childNodes(0)->childNodes(2)->childNodes(0)->plaintext)));
						$threads[$key]->pages=$pages;
						$threads[$key]->page=$page;
					} else {
						unset($threads[$key]);
						$key--;
						
					}
					
				} else {
					$key--;
				}
				$thread->clear();
				unset($thread);
				$key++;
			}
		}
		
		unset($html);
		
		
		//user stats
		$users=new ArrayObject();
		$uKey=0;
		
		foreach($threads as $thread){
			
			$exist=false;
			foreach($users as $k => $v){
				if ($users[$k]->user==$thread->user){
					$users[$k]->point+=1;
					
					if (str_word_count($thread->post)>=50)
						$users[$k]->point+=1;
					else {
						$users[$k]->point-=1;
						$users[$k]->not_passed+=1;
					}
					
					if (isset($thread->address) && $thread->address!="")
						$users[$k]->address=$thread->address;
					
					$exist=true;
					break;
				}
			}
			
			
			if (!$exist){
				if (!isset($users[$uKey]))
					$users[$uKey]=new stdClass;
				
				$users[$uKey]->user=$thread->user;
				if (str_word_count($thread->post)>=50){
					$users[$uKey]->point=1;
					$users[$uKey]->not_passed=0;
				} else {
					$users[$uKey]->point=0;
					$users[$uKey]->not_passed=1;
				}
				
				if (isset($thread->address) && $thread->address!="")
					$users[$uKey]->address=$thread->address;
				
				$uKey++;
				
			}
			
		}
		
		$users->uasort(function($a, $b) {
			return -($a->point - $b->point);
		});
		if ($data=="thread")
			echo json_encode($threads);
		else if ($data=="count"){
			echo count($threads);
		} else if ($data=="exec"){
			$dir = './files/btalk/';
			$files = array_diff(scandir($dir), array('.', '..'));
			
			if (!file_exists($dir.$phase.".json")){
				if (count($threads)==100){
					//create file
					$file = fopen($dir.(count($files)+1).".json", "w") or die("Unable to open file!");
					$users=array_values((array)$users);
					$winners=new ArrayObject();
					
					$position=1;
					foreach($users as $user){
						$winners[$position]=new stdClass;
						$winners[$position]->user=$user->user;
						$winners[$position]->point=$user->point;
						$winners[$position]->not_passed=$user->not_passed;
						$winners[$position]->amount=0;
						$winners[$position]->address="";
						$winners[$position]->status=false;
						if (isset($user->address)){
							$winners[$position]->address=$user->address;
							//send BLE
							$data=array(); 
							$data["jsonrpc"]=1.0;
							$data["method"]="sendtoaddress";
							
							if ($position==1)
								$amount=25;
							if ($position==2)
								$amount=15;
							if ($position==3)
								$amount=10;
							$data["params"]=array($user->address,$amount);
							$getTrx=(object)json_decode($this->function->curlRPC($data),true);
							
							if ($getTrx->result!=null){
								$winners[$position]->status=true;
								$winners[$position]->amount=$amount;
								$winners[$position]->message=$getTrx->result;
							} else {
								$winners[$position]->status=false;
								$winners[$position]->message=$getTrx->error["message"];
							}
							
						} else {
							$winners[$position]->message="invalid address or no address provided, don't forget in the next round";
						}
						$position++;
						if ($position>3)
							break;
					}
					
					fwrite($file, json_encode($winners));
					fclose($file);
					echo json_encode($winners);
				} else 
					echo "Phase ".$phase." is bellow 100 reply, try again later";
			} else {
				echo "phase ".$phase." has the winners";
			}
		} else if($data=="rank"){
			$users=array_values((array)$users);
			echo json_encode((object)$users);
		} else {
			$users=array_values((array)$users);
			echo json_encode((object)$users);
		}

	}
	
	public function tradeAction($source=null,$destination=null,$getPrice=false){
		if ($source=="ABS" && $destination=="BLE"){
			$this->view->title="BitcoinLE Trade | ABS/BLE";
			$this->view->description="Buy BitcoinLE (BLE) with AbsoluteCoin (ABS)";
			$this->view->rate=$rate=15.35;
			//$this->view->rate=$rate=1;
			if ($getPrice){
				$this->view->disable();
				echo $rate;
				exit;
			}
			
			$this->view->lists=Trades::find([
												'conditions' => ['pair' => $source.'/'.$destination],
												'sort'=>[   
													'time' => -1
												],
												'limit' => 10
											]);
		
			$this->view->pair="ABS";
			if ($this->request->isPost()){
				$this->view->disable();
				$response=new stdClass;
				$response->status=false;
				$clientIp=$this->request->getClientAddress();
				$gResponse=$this->request->getPost('captcha');
				
				//check chaptcha
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,"secret=6Lc1i3EUAAAAAIcY8Y37cP6lNKEFer28fM76HWT8&response=".$gResponse."&remoteip=".$clientIp);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$googleResponse = json_decode(curl_exec($ch));
				curl_close ($ch);
			
				if ($googleResponse->success==false){
					$response->message="Failed google recaptcha";
				} else {
					if (is_numeric($this->request->getPost('amount')) && $this->request->getPost('address')!=null && $this->request->getPost('address')!=""){
						//check ble address
						$data=array(); 
						$data["jsonrpc"]=1.0;
						$data["method"]="validateaddress";
						$data["params"]=array($this->request->getPost('address'));
						$ble=(object)json_decode($this->function->curlRPC($data),true)['result'];
						
						if ($this->request->getPost('amount')>15){
							$response->message="Max 15 BLE per transaction";
						} else {
							if ($ble->isvalid){
								//get address
								$data=array(); 
								$data["jsonrpc"]=1.0;
								$data["method"]="getnewaddress";
								$data["params"]=array();
								$addr=(object)json_decode($this->function->curlRPCExt($data,"rpcHost","rpcUser","rpcPass","rpcPort"),true);
								
								$random = new \Phalcon\Security\Random();
								$trades=new Trades();
								$trades->id=$random->uuid();
								$trades->pair=$source."/".$destination;
								$trades->pay_amount=$this->request->getPost('amount')*$rate;
								$trades->pay_address=$addr->result;
								$trades->pay_tx=null;
								$trades->paid_at=null;
								$trades->send_amount=$this->request->getPost('amount');
								$trades->send_address=$this->request->getPost('address');
								$trades->send_tx=null;
								$trades->sent_at=null;
								$trades->status="Waiting for Payment";
								$trades->time=time();
								$trades->save();
								
								$response->status=true;
								$response->id=$trades->id;
								$response->amount=$this->request->getPost('amount');
								$response->pay_amount=$this->request->getPost('amount')*$rate;
								$response->address=$this->request->getPost('address');
								$response->pay_address=$addr->result;
								$response->time=$trades->time;
								$response->status_info=$trades->status;
								
							} else $response->message="Invalid BLE Address";
						}
					} else {
						$response->message="Invalid amount or address";
					}
				}
				echo json_encode($response);
			}
		} else if ($source=="BTC" && $destination=="BLE"){
			$this->view->disable();
			echo "not available";
			$this->view->title="BitcoinLE Trade | BTC/BLE";
			$this->view->description="Buy BitcoinLE (BLE) with Bitcoin (BTC)";
			$this->view->rate=$rate=1200;
			$this->view->pair="BTC";
			if ($getPrice){
				$this->view->disable();
				echo $rate;
				exit;
			}
		} else if ($source=="WEB" && $destination=="BLE"){
			$this->view->title="BitcoinLE Trade | WEB/BLE";
			$this->view->description="Buy BitcoinLE (BLE) with Webchain (WEB)";
			$this->view->rate=$rate=97.5;
			//$this->view->rate=$rate=1;
			$this->view->pair="WEB";
			if ($getPrice){
				$this->view->disable();
				echo $rate;
				exit;
			}
			
			$this->view->lists=Trades::find([
												'conditions' => ['pair' => $source.'/'.$destination],
												'sort'=>[   
													'time' => -1
												],
												'limit' => 10
											]);
		
			if ($this->request->isPost()){
				$this->view->disable();
				$response=new stdClass;
				$response->status=false;
				$clientIp=$this->request->getClientAddress();
				$gResponse=$this->request->getPost('captcha');
				
				//check chaptcha
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,"secret=6Lc1i3EUAAAAAIcY8Y37cP6lNKEFer28fM76HWT8&response=".$gResponse."&remoteip=".$clientIp);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$googleResponse = json_decode(curl_exec($ch));
				curl_close ($ch);
			
				if ($googleResponse->success==false){
					$response->message="Failed google recaptcha";
				} else {
					if (is_numeric($this->request->getPost('amount')) && $this->request->getPost('address')!=null && $this->request->getPost('address')!=""){
						//check ble address
						$data=array(); 
						$data["jsonrpc"]=1.0;
						$data["method"]="validateaddress";
						$data["params"]=array($this->request->getPost('address'));
						$ble=(object)json_decode($this->function->curlRPC($data),true)['result'];
						
						if ($this->request->getPost('amount')>15){
							$response->message="Max 15 BLE per transaction";
						} else {
							if ($ble->isvalid){
								$unique=rand(1,10)/15;
								$random = new \Phalcon\Security\Random();
								$trades=new Trades();
								$trades->id=$random->uuid();
								$trades->pair=$source."/".$destination;
								$trades->pay_amount=round((($this->request->getPost('amount')*$rate)+$unique),4);
								$trades->pay_address="0x1474b66963cc35949d1d85d297311dd54e32bb7f";
								$trades->pay_tx=null;
								$trades->paid_at=null;
								$trades->send_amount=$this->request->getPost('amount');
								$trades->send_address=$this->request->getPost('address');
								$trades->send_tx=null;
								$trades->sent_at=null;
								$trades->status="Waiting for Payment";
								$trades->time=time();
								$trades->save();
								
								$response->status=true;
								$response->id=$trades->id;
								$response->amount=$this->request->getPost('amount');
								$response->pay_amount=$trades->pay_amount;
								$response->address=$this->request->getPost('address');
								$response->pay_address=$trades->pay_address;
								$response->time=$trades->time;
								$response->status_info=$trades->status;
								
							} else $response->message="Invalid BLE Address";
						}
					} else {
						$response->message="Invalid amount or address";
					}
				}
				echo json_encode($response);
			}
		} else if ($source=="detail" && $destination!=null){
			$this->view->title="BitcoinLE Trade Transaction";
			$this->view->description="Transaction Detail";
			if (preg_match('/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i', $destination)){
				$this->view->trade=$trade=Trades::findFirst(["conditions" => ["id" => $destination]]);
				if (isset($trade->id)){
					$this->view->pick(['ble/trade-detail']);
				} else {
					$this->view->disable();
					echo "not found";
				}
			} else {
				$this->view->disable();
				echo "not found";
			}
			
			
		} else {
			$this->view->disable();
			echo "not found";
		}
	}

}

