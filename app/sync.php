<?php

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Loader;

// Using the CLI factory default services container
$di = new CliDI();

/**
 * Register the autoloader and tell it to register the tasks directory
 */
$loader = new Loader();

$loader->registerDirs(
    [
        __DIR__ . '/tasks',
		__DIR__ . '/models',
		__DIR__ . '/library', 
    ]
);
$loader->registerNamespaces([
    'Phalcon' => __DIR__ . '/library/incubator/Library/Phalcon/'
] )->register();

$loader->register();

// Load the configuration file (if any)
$configFile = __DIR__ . '/config/config.php';

if (is_readable($configFile)) {
    $config = include $configFile;

    $di->set('config', $config);
}

$di->set('mongo', function () {
	$config = $this->getConfig();
	$host = $config->database->MongoDB->host;
	$username = $config->database->MongoDB->username;
	$password = $config->database->MongoDB->password;
	$port = $config->database->MongoDB->port;
	$dbname = $config->database->MongoDB->dbname;
	$mongo = new Phalcon\Db\Adapter\MongoDB\Client("mongodb://$username:$password@$host:$port/$dbname");
	return $mongo->selectDatabase($dbname); 
}, true);

$di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
}, true);

// Create a console application
$console = new ConsoleApp();

$console->setDI($di);

/**
 * Process the console arguments
 */
$arguments = [];

foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['task'] = $arg;
    } elseif ($k >= 2) {
        $arguments['params'][] = $arg;
    }
}

try {
    // Handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    // Do Phalcon related stuff here
    // ..
    fwrite(STDERR, $e->getMessage() . PHP_EOL);
    exit(1);
} catch (\Throwable $throwable) {
    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    exit(1);
} catch (\Exception $exception) {
    fwrite(STDERR, $exception->getMessage() . PHP_EOL);
    exit(1);
}