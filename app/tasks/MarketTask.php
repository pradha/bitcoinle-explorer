<?php
use Phalcon\Cli\Task;

class MarketTask extends Task{
	var $function;
	public function mainAction(array $params){
		$this->function=new Functions();
		$price=$this->function->curlJSON('https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,EUR,GBP,RUB,CNY,IDR');
		//print_r($price);
		if ($params[0]=="dxo"){
			//Graviex
			$data=$this->function->curlJSON('https://graviex.net/api/v2/tickers/dxobtc.json');
			if (count($data)>=1 && isset($data->at)){
				$check=Market::findFirst([
					'conditions' => ['coin' => 'DXO'],
					'sort' => ['time' => -1],
					'limit' => 20
				]);
				if ($data->ticker->last!=$check->ticker['last']){
					$market=new Market();
					$market->coin='DXO';
					$market->time=$data->at;
					$market->ticker=$data->ticker;
					$market->price=$price;
					$market->save();
				} 
			}
		} else if ($params[0]=="abs"){
			//Graviex
			$data=$this->function->curlJSON('https://graviex.net/api/v2/tickers/absbtc.json');
			if (count($data)>=1 && isset($data->at)){
				$check=Market::findFirst([
					'conditions' => ['coin' => 'ABS'],
					'sort' => ['time' => -1],
					'limit' => 20
				]);
				if ($data->ticker->last!=$check->ticker['last']){
					$market=new Market();
					$market->coin='ABS';
					$market->time=$data->at;
					$market->ticker=$data->ticker;
					$market->price=$price;
					$market->save();
				} 
			}
		} else if ($params[0]=="zoc" && isset($data->at)){
			//Graviex
			$data=$this->function->curlJSON('https://graviex.net/api/v2/tickers/zocbtc.json');
			if (count($data)>=1){
				$market=new Market();
				$market->coin='ZOC';
				$market->time=$data->at;
				$market->ticker=$data->ticker;
				$market->price=$price;
				$market->save();
					//echo "Market saved!".PHP_EOL;
			}
		}
		
		

	}
}