<?php

use Phalcon\Cli\Task;

class ValidateTask extends Task
{
	var $function;
	public function mainAction(array $params){
		$this->function=new Functions();
		echo "Task started at ".date("Y M d H:i:s").PHP_EOL;
		$start=time();
		if (isset($params[0])){
			$address=Addresses::findFirst(['conditions'=>['address' => $params[0]]]);
			if (isset($address->address)){
				echo "- Address found, lets check the transactions ".PHP_EOL;
				$transactions=Transactions::find([
					"conditions"=>[
						"address" => $params[0]
					],
					'sort'=>[
						'time' => 1
					]
				]);
				echo "-- This address has ".count($transactions)." Transactions :".PHP_EOL;
				$i=1;
				$vin=$vout=0;
				foreach($transactions as $transaction){
					$data=array(); 
					$data["jsonrpc"]=1.0;
					
					$data["method"]="getblock";
					$data["params"]=array($transaction->blockhash);
					$dataBlock=(object)json_decode($this->function->curlRPC($data),true)["result"];
					if (!isset($dataBlock->confirmations) || (isset($dataBlock->confirmations) && $dataBlock->confirmations<0)){
						echo "--> ".$transaction->blockhash." ".$transaction->position." ".$transaction->value.PHP_EOL; 
						echo "--> Block is orphaned".PHP_EOL;
						if ($transaction->position=="vin")
							$vin=$vin+$transaction->value;
						else
							$vout=$vout+$transaction->value;
					} else {
						//check transaction
						$data["method"]="getrawtransaction";
						$data["params"]=array($transaction->txid,1);
						$dataTrx=(object)json_decode($this->function->curlRPC($data),true)["result"];
						if (!isset($dataTrx->confirmations) || (isset($dataTrx->confirmations) && $dataTrx->confirmations<0)){
							echo "--> ".$transaction->txid." ".$transaction->position." ".$transaction->value.PHP_EOL; 
							if (isset($dataTrx->confirmations) && $dataTrx->confirmations<0)
								echo "--> Confirmation(s) : ".$dataTrx->confirmations.PHP_EOL;
							else
								echo "--> Confirmation(s) : Orphan".PHP_EOL;
						}
					}
					
					$i++;
				}
				echo "Result : ".PHP_EOL;
				echo "Total invalid received amount: ".$vin.PHP_EOL;
				echo "Total invalid sent amount: ".$vout.PHP_EOL;
				$address=Addresses::findFirst(['conditions'=>['address' => $params[0]]]);
				echo "Total balance ".$address->address." is ".$address->balance." it must be ".($address->balance-$vout+$vin).PHP_EOL;
			} else {
				echo "-- Address not found ".$params[0].PHP_EOL;
			}
		} else {
			echo "Oops..!address parameter is required".PHP_EOL;
		}
		$end=time();
		echo "Task Finished at ".date("Y M d H:i:s")." ".($end-$start)." seconds".PHP_EOL;
	
	}
}