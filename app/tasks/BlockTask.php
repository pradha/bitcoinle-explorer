<?php

use Phalcon\Cli\Task;

class BlockTask extends Task
{
	var $function;
	public function mainAction(array $params){
		$this->function=new Functions();
		echo "Task started at ".date("Y M d H:i:s").PHP_EOL;
		
		$data=array(); 
		$data["jsonrpc"]=1.0;
		
		//check last block
		$data["method"]="getblockchaininfo";
		$data["params"]=array();
		
		$result=json_decode($this->function->curlRPC($data),true);
		echo "last main block : " . $result["result"]["blocks"].PHP_EOL;
		echo PHP_EOL;
		$lastBlock=$result["result"]["blocks"];
		
		$checkBlock=Transactions::findFirst([
			'sort'=>[
				'blockheight' => -1
			],
			'limit' => 1,
		]);
		//echo "block db ".$checkBlock->blockheight.PHP_EOL;
		if (isset($checkBlock->blockheight))
			$dbBlock=$checkBlock->blockheight;
		else
			$dbBlock=0;
		
		if (isset($params[0]) && is_numeric($params[0]) && isset($params[1]) && is_numeric($params[1])){
			$dbBlock=$params[0];
			$lastBlock=$params[1];
		}
		
		/*$logFileName="sync_".date('Ymd_His')."_".$dbBlock."_".$lastBlock.".log";
		$log=BASE_PATH."/public/log/".$logFileName;
		
		if (file_exists($log)===false){
			$fh = fopen($log, 'w');
			fclose($fh);
			chmod($log, 0777);
		}*/
		
		while ($dbBlock <= $lastBlock) {
			//2825 5627
			//5144 5158 5606 5627 6046 6404 6437 89778 89827
			//3465 5165 multiple vouts
			//110110 110123 110203 110212 110224 110313 D82mdhN84kRZoTFz9vNb9zPEcbB1z2E1sU
			//104724 105204 D9fQmnW78L3WdTd92B4oUh7iQprVF7UGLx
			/*if ($dbBlock!=104724 && $dbBlock!=105204 && $dbBlock!=105306 && $dbBlock!=105418 && $dbBlock!=105472 && $dbBlock!=105659 && $dbBlock!=105800 && $dbBlock!=105888 && $dbBlock!=105915 && $dbBlock!=106191
				&& $dbBlock!=106291 && $dbBlock!=106332 && $dbBlock!=106437 && $dbBlock!=106480 && $dbBlock!=106826 && $dbBlock!=106928 && $dbBlock!=107256 && $dbBlock!=107305 && $dbBlock!=107383 && $dbBlock!=107441
				&& $dbBlock!=107818 && $dbBlock!=107845 && $dbBlock!=107920 && $dbBlock!=108067 && $dbBlock!=108365 && $dbBlock!=108509 && $dbBlock!=108524 && $dbBlock!=108595 && $dbBlock!=108638 && $dbBlock!=108793
				&& $dbBlock!=108889 && $dbBlock!=108892 && $dbBlock!=108985 && $dbBlock!=109067 && $dbBlock!=109087 && $dbBlock!=109183 && $dbBlock!=109265 && $dbBlock!=109337 && $dbBlock!=109428 && $dbBlock!=109525
				&& $dbBlock!=109968 && $dbBlock!=110146 && $dbBlock!=110258 && $dbBlock!=110285 && $dbBlock!=110365
			){ 
				//echo "Skipping block ".$dbBlock.PHP_EOL;
				$dbBlock++;
				continue;
			}*/
			echo "Processing Block ".$dbBlock.PHP_EOL;
			
			$data["method"]="getblockhash";
			$data["params"]=array(intval($dbBlock));
			$hashBlock=json_decode($this->function->curlRPC($data),true);
			
			if ($hashBlock==null){
				echo "Block not found exit thread".PHP_EOL;
				break;
			}
			
			//get block details
			$data["method"]="getblock";
			$data["params"]=array($hashBlock["result"]);
			$dataBlock=json_decode($this->function->curlRPC($data),true)["result"];

			
			foreach($dataBlock["tx"] as $trx){
				$data["method"]="getrawtransaction";
				$data["params"]=array($trx,1);
				$dataTrx=json_decode($this->function->curlRPC($data),true)["result"];
				
				$vinObject= new ArrayObject();
				if (!empty($dataTrx["vin"])){
					echo "- Retrieve vin trx (".count($dataTrx["vin"])." trxs)".PHP_EOL;
					foreach($dataTrx["vin"] as $vin){
						if (!isset($vin["coinbase"])){
							$data["params"]=array($vin["txid"],1);
							$resultVin=json_decode($this->function->curlRPC($data),true)["result"];
						}
							
						$exist=false;
						foreach($vinObject as $key => $value){
							if (isset($vin["coinbase"])){
								if ($value["address"]=="coinbase"){
									$exist=true;
									break;
								}
							} else {
								if ($value->address==$this->searchVout($vin["vout"],$resultVin["vout"])["scriptPubKey"]["addresses"][0]){
									$exist=true;
									$value->value+=$this->searchVout($vin["vout"],$resultVin["vout"])["value"];
									break;
								}
							}
						}
					
							
						if ($exist===false){
							$vinDetail=new ArrayObject();
							$vinDetail->txid=$dataTrx["txid"];
							$vinDetail->blockhash=$dataTrx["blockhash"];
							$vinDetail->blockheight=$dataBlock["height"];
							$vinDetail->time=$dataTrx["time"];
							if (isset($vin["coinbase"])){
								$vinDetail->address="coinbase";
								$vinDetail->value=0;
								$vinDetail->position="new coins";
							} else {
								$vinDetail->address=$this->searchVout($vin["vout"],$resultVin["vout"])["scriptPubKey"]["addresses"][0];
								$vinDetail->value=$this->searchVout($vin["vout"],$resultVin["vout"])["value"];
								$vinDetail->position="vin";
							}
							
							$vinObject->append($vinDetail);
						}
					}
				}
				
				//save vin transaction
				echo "- Save vin transactions".PHP_EOL;
				foreach($vinObject as $vin){
					echo "-- Save address ".$vin->address.PHP_EOL;
					$transaction=Transactions::findFirst(['conditions'=>['txid' => $vin->txid,'time' => $vin->time, 'address' => $vin->address, 'position' => 'vin']]);
					if (!isset($transaction->txid)){
						
						//check address if exists in vout POS reward / simillar
						$isPos=false;
						foreach($dataTrx["vout"] as $key => $value){
							if (isset($value["scriptPubKey"]["addresses"])){
								if ($value["scriptPubKey"]["type"]!="nonstandard" && $value["scriptPubKey"]["addresses"][0]==$vin->address && count($vinObject)==1 && $value["scriptPubKey"]["type"]=="pubkey"){
									$isPos=true;
									break;
								}
							}
						}
						if ($isPos===false){
							//check and save address
							$addresses=Addresses::findFirst(['conditions'=>['address' => $vin->address]]);
							if (!isset($addresses->address)){
								$addresses=new Addresses();
								$addresses->address=$vin->address;
								$addresses->received=$addresses->sent=$addresses->balance=0;
								$addresses->alias="";
							}
							$addresses->sent=($addresses->sent+$vin->value);
							$addresses->balance=($addresses->balance-$vin->value);
							$addresses->address=$vin->address;
							$addresses->save();
						}
						$transaction=new Transactions();
					}
					if ($isPos===false){
						$transaction->txid=$vin->txid;
						$transaction->blockhash=$vin->blockhash;
						$transaction->blockheight=$vin->blockheight;
						$transaction->time=$vin->time;
						$transaction->address=$vin->address;
						$transaction->value=$vin->value;
						$transaction->position=$vin->position;
						$transaction->save();
					}
				}
				
				
				$voutObject=new ArrayObject();
				if (!empty($dataTrx["vout"])){
					echo "- Retrieve vout trx (".count($dataTrx["vout"])." trxs)".PHP_EOL;
					foreach($dataTrx["vout"] as $vout){
						if (isset($vout["scriptPubKey"]["addresses"])){
							$exist=false;
							foreach($voutObject as $key => $value){
								if ($value->address==$vout["scriptPubKey"]["addresses"][0]){
									$exist=true;
									$value->value+=$vout["value"];
									break;
								}
							}
							
							if ($exist===false){
								$voutDetail=new ArrayObject();
								$voutDetail->txid=$dataTrx["txid"];
								$voutDetail->blockhash=$dataTrx["blockhash"];
								$voutDetail->blockheight=$dataBlock["height"];
								$voutDetail->time=$dataTrx["time"];
								
								if (isset($vout["value"]) && isset($vout["n"]) && $vout["scriptPubKey"]["type"]!="nonstandard") {
									//check if PoS Tx
									$isPos=false;
									$vinValue=0;
									foreach($vinObject as $key => $value){
										if ($value->address==$vout["scriptPubKey"]["addresses"][0] && count($vinObject)==1 && $vout["scriptPubKey"]["type"]=="pubkey"){
											$isPos=true;
											$vinValue=$value->value;
											break;
										}
										
									}
									
									if ($isPos)
										$voutDetail->value=$vout["value"]-$vinValue;
									else
										$voutDetail->value=$vout["value"];
									$voutDetail->address=$vout["scriptPubKey"]["addresses"][0];
									$voutDetail->position="vout";
									$voutObject->append($voutDetail);
								}
							}
						}
					}
				}
				
				//print_r($voutObject);
				//save transaction
				echo "- Save vout transactions".PHP_EOL;
				foreach($voutObject as $vout){
					echo "-- Save address ".$vout->address.PHP_EOL;
						$transaction=Transactions::findFirst(['conditions'=>['txid' => $vout->txid,'time' => $vout->time, 'address' => $vout->address, 'position' => 'vout']]);
						if (!isset($transaction->txid)){
							$addresses=Addresses::findFirst(['conditions'=>['address' => $vout->address]]);
							if (!isset($addresses->address)){
								$addresses=new Addresses();
								$addresses->received=$addresses->sent=$addresses->balance=0;
								$addresses->alias="";
							}
							$addresses->received=($addresses->received+$vout->value);
							$addresses->balance=($addresses->balance+$vout->value);
							$addresses->address=$vout->address;
							$addresses->save();
							
							$transaction=new Transactions();
						}
						$transaction->txid=$vout->txid;
						$transaction->blockhash=$vout->blockhash;
						$transaction->blockheight=$vout->blockheight;
						$transaction->time=$vout->time;
						$transaction->address=$vout->address;
						$transaction->value=$vout->value;
						$transaction->position=$vout->position;
						$transaction->save();
				}
					
			}
			echo "- Block height ".$dbBlock." done ".PHP_EOL;
			echo "- Progress ".number_format($dbBlock/$lastBlock*100,2)."%".PHP_EOL;
			
			//error_log(date('Y-m-d H:i:s')." Save Block ".$dbBlock." => ".date('Y-m-d H:i:s', $dataTrx["time"])." | Progress ".number_format($dbBlock/$lastBlock*100,2)."%\n", 3,$log);
			$dbBlock++; 
		}
		echo "Finished".PHP_EOL;
		
	}	
	
	function searchVout($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['n'] === $id) {
				return $val;
			}
		}
		return null;
	}
}