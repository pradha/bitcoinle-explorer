<?php

use Phalcon\Cli\Task;

class MainTask extends Task
{
    public function mainAction()
    {
        echo 'This is the default task and the default action' . PHP_EOL;
    }
	
	public function updateAction(array $params){
		ob_start();
		
		$data=array();
		$data["jsonrpc"]=1.0;
		
		//check last block
		$data["method"]="getinfo";
		$data["params"]=array();
		
		$result=json_decode($this->curl($data),true);
		echo "last main block : " . $result["result"]["blocks"].PHP_EOL;
		echo PHP_EOL;
		$lastBlock=$result["result"]["blocks"];
		
		$checkBlock=Blocks::findFirst([
			'sort'=>[
				'height' => -1
			],
			'limit' => 1,
		]);
		if (isset($checkBlock->height))
			$dbBlock=$checkBlock->height;
		else
			$dbBlock=0;
		
		if (isset($params[0]) && is_numeric($params[0]) && isset($params[1]) && is_numeric($params[1])){
			$dbBlock=$params[0];
			$lastBlock=$params[1];
		}
		
		//$logFileName="sync_".date('Ymd_His')."_".$dbBlock."_".$lastBlock.".log";
		//$log=BASE_PATH."/public/log/".$logFileName;
		
		if (file_exists($log)===false){
			$fh = fopen($log, 'w');
			fclose($fh);
			chmod($log, 0777);
		}
		
		while ($dbBlock <= $lastBlock) {
			//get block hash
			echo "Processing Block ".$dbBlock."\r\n";
			ob_flush();
			//error_log(date('Y-m-d H:i:s')." Processing Block ".$dbBlock."\n", 3, $log);
			
			$data["method"]="getblockhash";
			$data["params"]=array($dbBlock);
			$hashBlock=json_decode($this->curl($data),true);
			
			if ($hashBlock==null){
				echo "Block not found exit thread".PHP_EOL;
				break;
			}
			
			//get block details
			$data["method"]="getblock";
			$data["params"]=array($hashBlock["result"]);
			$dataBlock=json_decode($this->curl($data),true)["result"];
			
			
			
			//check block
			$block=Blocks::findFirst(['conditions'=>['hash' => $dataBlock["hash"]]]);
			//save block
			if (!isset($block->hash))
				$block=new Blocks();
			
			$block->hash=$dataBlock["hash"];
			$block->confirmation=$dataBlock["confirmations"];
			$block->size=$dataBlock["size"];
			$block->height=$dataBlock["height"];
			$block->version=$dataBlock["version"];
			$block->merkleroot=$dataBlock["merkleroot"];
			$block->tx=$dataBlock["tx"];
			$block->time=$dataBlock["time"];
			$block->nonce=$dataBlock["nonce"];
			$block->bits=$dataBlock["bits"];
			$block->difficulty=$dataBlock["difficulty"];
			$block->chainwork=$dataBlock["chainwork"];
			$block->nextblockhash=$dataBlock["nextblockhash"];
			$block->save();
			
			foreach($dataBlock["tx"] as $trx){
				//get raw transaction
				echo "- Retrieve transaction hash ".$trx."\n";
				ob_flush();
				//error_log(date('Y-m-d H:i:s')." Retrieve transaction hash ".$trx."\n", 3, $log);
				
				$data["method"]="getrawtransaction";
				$data["params"]=array($trx,1);
				$dataTrx=json_decode($this->curl($data),true)["result"];
				
				$newVin=array();
				
				if (!empty($dataTrx["vin"])){
					echo "- Retrieve vin trx (".count($dataTrx["vin"])." trxs)\n";
					ob_flush();
					$count=0;
					foreach($dataTrx["vin"] as $vin){
						$count++;
						if (!isset($vin["coinbase"])){
							$newDataVin=array();
							$newDataVin["txid"]=$vin["txid"];
							$newDataVin["vout"]=$vin["vout"];
							$newDataVin["scriptSig"]=array(
								"asm"=>$vin["scriptSig"]["asm"],
								"hex"=>$vin["scriptSig"]["hex"]
							);
							$newDataVin["sequence"]=$vin["sequence"];
						
							//get vin transaction
							$data["params"]=array($vin["txid"],1);
							$resultVin=json_decode($this->curl($data),true)["result"];
						
							$newDataVin["addresses"]=$this->searchVout($vin["vout"],$resultVin["vout"])["scriptPubKey"]["addresses"][0];
							$newDataVin["value"]=$this->searchVout($vin["vout"],$resultVin["vout"])["value"];
							
							array_push($newVin,$newDataVin);
							
							echo "- Save/update vin trx addr ".$newDataVin["addresses"]." (".$count."/".count($dataTrx["vin"]).") "."\n";
							ob_flush();
							$addrTrx=AddressTransactions::findFirst(['conditions'=>['txid' => $dataTrx["txid"],'time' => $dataTrx["time"], 'address' => $newDataVin["addresses"]]]);
							if (!isset($addrTrx->txid)){
								//not exist in transaction then save/update the address balance
								$addresses=Addresses::findFirst(['conditions'=>['address' => $newDataVin["addresses"]]]);
								if (!isset($addresses->address)){
									$addresses=new Addresses();
									$addresses->received=$addresses->sent=0;
								}
								$addresses->address=$newDataVin["addresses"];
								$addresses->sent=$addresses->sent+$newDataVin["value"];
								$addresses->balance=$addresses->received-$addresses->sent;
								$addresses->save();
								
								//create new address transaction
								$addrTrx=new AddressTransactions();
							}
							$addrTrx->txid=$dataTrx["txid"];
							$addrTrx->blockhash=$dataTrx["blockhash"];
							$addrTrx->blockheight=$dataBlock["height"];
							$addrTrx->time=$dataTrx["time"];
							$addrTrx->address=$newDataVin["addresses"];
							$addrTrx->value=$newDataVin["value"];
							$addrTrx->type="vin";
							$addrTrx->save();
						}
					}
				}
				
				if (!empty($dataTrx["vout"])){
					echo "- Retrieve vout trx (".count($dataTrx["vout"])." trxs)\n";
					ob_flush();
					$count=0;
					foreach($dataTrx["vout"] as $vout){
						$count++;
						if (isset($vout["value"]) && isset($vout["n"]) && $vout["scriptPubKey"]["type"]!="nonstandard"){
							echo "- Save/update vout trx addr ".$vout["scriptPubKey"]["addresses"][0]." (".$count."/".count($dataTrx["vout"]).") ".PHP_EOL;
							ob_flush();
							$addrTrx=AddressTransactions::findFirst(['conditions'=>['txid' => $dataTrx["txid"],'time' => $dataTrx["time"], 'address' => $vout["scriptPubKey"]["addresses"][0]]]);
							if (!isset($addrTrx->txid)){
								//not exist in transaction then save/update the address balance
								$addresses=Addresses::findFirst(['conditions'=>['address' => $vout["scriptPubKey"]["addresses"][0]]]);
								if (!isset($addresses->address)){
									$addresses=new Addresses();
									$addresses->sent=$addresses->received=0;
								}
								$addresses->address=$vout["scriptPubKey"]["addresses"][0];
								$addresses->received=$addresses->received+$vout["value"];
								$addresses->balance=$addresses->received-$addresses->sent;
								$addresses->save();
								
								//create new address transaction
								$addrTrx=new AddressTransactions();
							}
							$addrTrx->txid=$dataTrx["txid"];
							$addrTrx->blockhash=$dataTrx["blockhash"];
							$addrTrx->blockheight=$dataBlock["height"];
							$addrTrx->time=$dataTrx["time"];
							$addrTrx->address=$vout["scriptPubKey"]["addresses"][0];
							$addrTrx->value=$vout["value"];
							$addrTrx->type="vout";
							$addrTrx->save(); 
						}
					}
				}
				
				//save transaction
				echo "- Finally save trx ".$dataTrx["txid"].PHP_EOL;
				ob_flush();
				$transaction=Transactions::findFirst(['conditions'=>['txid' => $dataTrx["txid"]]]);
				if (!isset($transaction->txid))
					$transaction=new Transactions();
				$transaction->txid=$dataTrx["txid"];
				$transaction->version=$dataTrx["version"];
				$transaction->locktime=$dataTrx["locktime"];
				$transaction->vin=$newVin;
				$transaction->vout=$dataTrx["vout"];
				$transaction->blockhash=$dataTrx["blockhash"];
				$transaction->confirmations=$dataTrx["confirmations"];
				$transaction->time=$dataTrx["time"];
				$transaction->blocktime=$dataTrx["blocktime"];
				$transaction->save();
			}
			echo "- Block height ".$dbBlock." done ".PHP_EOL;
			echo "- Progress ".number_format($dbBlock/$lastBlock*100,2)."%".PHP_EOL;
			ob_flush();
			//error_log(date('Y-m-d H:i:s')." Save Block ".$dbBlock." => ".date('Y-m-d H:i:s', $dataTrx["time"])." | Progress ".number_format($dbBlock/$lastBlock*100,2)."%\n", 3,$log);
			$dbBlock++; 
		}
		echo "Finished";
		ob_end_flush();
	}
	
	function curl($data){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://rpcUser:rpcPass@rpcHost:rpcPort/");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		return $result;
	}
	
	function searchVout($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['n'] === $id) {
				return $val;
			}
		}
		return null;
	}
}