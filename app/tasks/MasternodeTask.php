<?php

use Phalcon\Cli\Task;

class MasternodeTask extends Task
{
	var $function;
	public function mainAction(array $params){
		$this->function=new Functions();
		$data["method"]="masternode";
		$data["params"]=array("list","full");
		$result=(object)json_decode($this->function->curlRPC($data),true)['result'];
		
		$mastenodes=new ArrayObject();
		foreach($result as $key => $value){
			$mnLists=new ArrayObject();
			$detail=explode(" ",preg_replace('/\s+/', ' ',trim($value)));
			
			$mnLists->txid=$key;
			$mnLists->status=$detail[0];
			$mnLists->protocol=$detail[1];
			$mnLists->pubkey=$detail[2];
			$mnLists->lastseen=$detail[3];
			$mnLists->activeseconds=$detail[4];
			$mnLists->lastpaid=$detail[5];
			$mnLists->lastpaidblock=$detail[6];
			$mnLists->ip=$detail[7];
			
			
			
			
			$mastenodes->append($mnLists);
			
			
			echo "Processing Masternode ".$key.PHP_EOL;
			$masternode=Masternodes::findFirst([
				'conditions'=> ['txid' => $key]
			]);
			if (!isset($masternode->txid)){
				$masternode=new Masternodes();
				$country=$this->function->ip2country($detail[7]);
			} else {
				echo "Masternode Exist, Update".PHP_EOL;
				$country=$masternode->country;
				if ($country==""){
					echo "check ip to country ".$detail[7].PHP_EOL;
					$country=$this->function->ip2country($detail[7]);
				}
				
			}
			
			$masternode->txid=$key;
			$masternode->status=$detail[0];
			$masternode->protocol=$detail[1];
			$masternode->pubkey=$detail[2];
			$masternode->lastseen=$detail[3];
			$masternode->activeseconds=$detail[4];
			$masternode->lastpaid=$detail[5];
			$masternode->lastpaidblock=$detail[6];
			$masternode->ip=$detail[7];
			$masternode->country=$country;
			
			
			$masternode->lastpaid=$detail[6];
			$masternode->save();
		}
		
		$mnDb=Masternodes::find();
		echo "Processing masternode in db ".count($mnDb).PHP_EOL;
		foreach($mnDb as $mn){
			$exist=false;
			foreach($mastenodes as $key => $value){
				if ($value->txid==$mn->txid){
					$exist=true;
					break;
				}
			}
			
			if ($exist){
				/*echo "MN Exist IP:".$mn->ip.' Country: '.$mn->country.PHP_EOL;
				if ($mn->country!=""){
					echo "Empty country".PHP_EOL;
					if (strpos($ip, ']') !== false) {
						$ip =  trim(explode("]",$ip)[0],"[");
					} else {
						$ip =  explode(":",$ip)[0];
					}
					
					if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {    
						$s = file_get_contents('http://ip2c.org/'.$ip);
						switch($s[0]){
							case '0':
								echo $mn->ip.' Something wrong'.PHP_EOL;
								break;
							case '1':
								$reply = explode(';',$s);
								$mn->country=$reply[1];
								echo $mn->ip ."country updated".PHP_EOL;
								$mn->save();
								break;
							case '2':
								echo $mn->ip.' Not found in database'.PHP_EOL;
								break;
						}
					} else if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {   
						$s = file_get_contents('https://api.ipdata.co/'.$ip.'?api-key=07bbc0ba49891f2522bfbbdbf3c54b9a971d384fb163e2dfa32447b4');
						$country=json_decode($s,true);
						$mn->country=$country["country_code"];
						$mn->save();
						echo $mn->ip ."country updated".PHP_EOL;
					}
				}*/
			} else {
				/*$delMN=Masternodes::findFirst([
					['txid' => $mn->txid]
				])->delete();*/				
				echo $mn->txid ."deleted".PHP_EOL;
				$mn->delete();
			}
			
		}
		echo "Masternode Sync Finished!!".PHP_EOL;
	}
}