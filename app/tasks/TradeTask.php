<?php
use Phalcon\Cli\Task;

class TradeTask extends Task{
	var $function;
	public function mainAction(array $params){
		$this->function=new Functions();
		$trades=Trades::find(["conditions" => ["status" => ['$nin' => ['Completed','Expired']]]]);
		echo "Found ".count($trades)." Transactions".PHP_EOL;
		foreach($trades as $trade){
			echo "Processing trade ".$trade->id.PHP_EOL;
			echo "- Pair ".$trade->pair.PHP_EOL;
			echo "- Pay To ".$trade->pay_address.PHP_EOL;
			echo "- Amount ".$trade->pay_amount.PHP_EOL;
			echo "- Checking transaction...".PHP_EOL;
			
			if ($trade->pair=="ABS/BLE"){				
				$data=array(); 
				$data["jsonrpc"]=1.0;
				$data["method"]="listtransactions";
				$data["params"]=array("*",10);
				$trxs=(object)json_decode($this->function->curlRPCExt($data,"127.0.0.1","pradha","bismillahirrahmanirrahim","18889"),true)['result'];
				
				//print_r($trxs);
				$found=false;
				$confirmations=0;
				foreach($trxs as $trx){
					if ($trx["address"]==$trade->pay_address && $trx["amount"]==$trade->pay_amount && $trx["category"]=="receive"){
						echo "  Transaction found ".$trx["txid"].PHP_EOL;
						echo "  Updating DB... ";
						$trade->pay_tx=$trx["txid"];
						$trade->paid_at=$trx["time"];
						
						$confirmations=$trx["confirmations"];
						if ($trx["confirmations"]<6)
							$trade->status="Waiting for Confirmation";
						$trade->save();
						echo "Done. ".PHP_EOL;
						
						
						$found=true;
						break;
					}
				}
				
				if (!$found)
					echo "  Transaction not found ".PHP_EOL;
				else {
					if ($confirmations>=6){
						echo "  Sending BLE... ";
						$data=array(); 
						$data["jsonrpc"]=1.0;
						$data["method"]="sendtoaddress";
						$data["params"]=array($trade->send_address,$trade->send_amount);
						$getTrx=(object)json_decode($this->function->curlRPC($data),true);
						if ($getTrx->result!=null){
							$trade->send_tx=$getTrx->result;
							$trade->sent_at=time();
							$trade->status="Completed";
							$trade->save();
						}
					} else {
						echo "  Not enough confirmation, skipping... ".PHP_EOL;
					}
				}
				
				if ((time()-$trade->time)>=3600){
					echo "- Set expired...Done ".PHP_EOL;
					$trade->status="Expired";
					$trade->save();
				}
			} else if ($trade->pair=="WEB/BLE"){
				$ch = curl_init("https://explorer.webchain.network/addr");
				$data = array(
					'start' => 0,
					'length' => 5,
					'addr' => "0x1474b66963cc35949d1d85d297311dd54e32bb7f"
				);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen(json_encode($data)))
				);
				curl_setopt($ch, CURLOPT_TIMEOUT, 20);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
				$result = curl_exec($ch);
				curl_close($ch);
				
				$trxs=(object)json_decode($result,true)["data"];
				$found=false;
				foreach ($trxs as $trx){
					if ($trx[3]=="0x1474b66963cc35949d1d85d297311dd54e32bb7f" && $trx[4]==$trade->pay_amount){
						echo "  Transaction found ".$trx[0].PHP_EOL;
						echo "  Updating DB... ";
						$trade->pay_tx=$trx[0];
						$trade->paid_at=$trx[6];
						$trade->save();
						echo "  Done.".PHP_EOL;
						
						echo "  Sending BLE... ";
						$data=array(); 
						$data["jsonrpc"]=1.0;
						$data["method"]="sendtoaddress";
						$data["params"]=array($trade->send_address,$trade->send_amount);
						$getTrx=(object)json_decode($this->function->curlRPC($data),true);
						if ($getTrx->result!=null){
							$trade->send_tx=$getTrx->result;
							$trade->sent_at=time();
							$trade->status="Completed";
							$trade->save();
							echo "  Done.".PHP_EOL;
						} else {
							echo "  Failed.".PHP_EOL;
						}
						
						
						$found=true;
						break;
						/*echo "  Transaction ID : ".$trx[0].PHP_EOL;
						echo "  Block Number : ".$trx[1].PHP_EOL;
						echo "  Addr Source : ".$trx[2].PHP_EOL;
						echo "  Addr Destination : ".$trx[3].PHP_EOL;
						echo "  Amount : ".$trx[4]." WEB".PHP_EOL;
						echo "  Gas : ".$trx[5].PHP_EOL;
						echo "  time : ".$trx[6].PHP_EOL;*/
					}
				}
				if (!$found)
					echo "  Transaction not found ".PHP_EOL;
				
				if ((time()-$trade->time)>=3600){
					echo "- Set expired...Done ".PHP_EOL;
					$trade->status="Expired";
					$trade->save();
				}
				
				
			}
			
			
		}
	}
}