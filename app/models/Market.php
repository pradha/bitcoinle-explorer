<?php

use Phalcon\Mvc\MongoCollection;

class Market extends MongoCollection
{
	public function initialize()
    {
        $this->setSource('market');
    }
}