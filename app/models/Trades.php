<?php

use Phalcon\Mvc\MongoCollection;

class Trades extends MongoCollection
{
	public function initialize()
    {
        $this->setSource('trades');
    }
}
