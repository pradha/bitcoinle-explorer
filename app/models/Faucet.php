<?php

use Phalcon\Mvc\MongoCollection;

class Faucet extends MongoCollection
{
	public function initialize()
    {
        $this->setSource('faucet');
    }
}
