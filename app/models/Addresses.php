<?php

use Phalcon\Mvc\MongoCollection;

class Addresses extends MongoCollection
{
	public function initialize()
    {
        $this->setSource('addresses');
    }
}
