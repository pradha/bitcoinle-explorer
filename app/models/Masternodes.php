<?php

use Phalcon\Mvc\MongoCollection;

class Masternodes extends MongoCollection
{
	public function initialize()
    {
        $this->setSource('masternodes');
    }
}
