<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
    'database' => [
		'PostgreSQL' => [
			'adapter'     => 'Postgresql',
			'host'        => 'localhost',
			'username'    => '',
			'password'    => '',
			'dbname'      => '',
			'schema'      => ''
		],
		'MongoDB' => [
			'host'		=> 'localhost',
			'username'	=> 'userDB',
			'password'	=> 'passDB',
			'dbname' 	=> 'dbName',
			'port'		=> 'dbPort'
		]
	],
	'blockchain' =>	[
		'host'		=> '127.0.0.1',
		'username'	=> 'rpc_user',
		'password'	=> 'rpc_password',
		'port'		=> 'rpc_port',
		'ticker'	=> 'BLE',
		'masternodeCollateral'	=> 0
	],
    'application' => [
		'name'			=> 'BitcoinLE (BLE) Block Explorer',
		'theme'			=> 'yellow',
		'priceChartColor'	=> 'rgba(251,189,8,1)',
		'priceChartBackgroundColor'	=> 'rgba(251,189,8,0.1)',
		'networkChartColor'	=> 'rgba(251,189,8,1)',
		'networkChartBackgroundColor'	=> 'rgba(251,198,8,0.1)',
		'mapcolor'		=> "#E6E6FA",
		'mapScale'		=> "'#EE82EE', '#8A2BE2'",
		'menu'			=> array('Top 100','Network'),// menu can be Top 100 | Masternode | Network | Api
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',

        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        'baseUri'        => 'https://ble.block-explorer.club',
    ]
]);
